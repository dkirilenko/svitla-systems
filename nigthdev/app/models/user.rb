class User < ActiveRecord::Base
    validates :email, uniqueness: {message: "Email already used"}
    #before_save :encrypt_password

    def self.authenticate(email, password)
        user = find_by_email(email)
        user = (user && user.password == Digest::MD5.hexdigest(password)) ? user : nil
    end

    def self.encrypt_password(pass)
        Digest::MD5.hexdigest(pass)
    end
end
