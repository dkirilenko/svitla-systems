class AccountController < ApplicationController
  protect_from_forgery

  def index

  end

  def new

  end

  def create
    user = User.new({
        :username => params[:user][:username],
        :first_name => params[:user][:first_name],
        :last_name => params[:user][:last_name],
        :email => params[:user][:email],
        :password => User.encrypt_password(params[:user][:password])
    })

    if user.save
        session[:user_id] = user.id
        redirect_to controller:"home", action: "index"
    else
        render :new
    end
  end

  def logging_user
    user = User.authenticate(params[:email], params[:password])
    if user
        session[:user_id] = user.id
        redirect_to controller:"home", action:"index"
    else
        render :index
    end

  end

  def destroy
    session.delete(:user_id)
    render :index
  end

end
