// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .
function saveUserData() {
    var username = $('#username').val()
    var first_name = $('#first_name').val()
    var last_name = $('#last_name').val()
    var email = $('#email').val()
    var password = $('#password').val()
    var newPassword = $('#new_password').val()
    var confirmPassword = $('#confirm_password').val()
    var errorMessage = "";
    var isValid = true;
    if (newPassword != confirmPassword) {
        $('#new_password, #confirm_password').addClass('borderRed')
        errorMessage = "New password does not equals confirm password"
        isValid = false;
    }

    if (!stringIsNotEmpty(password)) {
        $('#password').addClass('borderRed')
        errorMessage = "Set password"
        isValid = false;
    }

    var data = {
        username : username,
        first_name : first_name,
        last_name : last_name,
        email : email,
        password : password,
        new_password : newPassword,
    }
    if (isValid) {
        $.post("/users/update", data, function(data) {
            if (data.status == "done") {
                $('#new_password, #confirm_password, #password').val("")
                alert("Done")
            } else {
                $.each(data.message, function(index, message) {
                    errorMessage += message + " "
                })

                $('#errorMessage').text(errorMessage);
            }
        })
    }
    $('#errorMessage').text(errorMessage);

}

function stringIsNotEmpty(str) {
    return str && $.type(str) === 'string' && str.length > 0
}