system "clear"

class Base
  def executeTask
    puts "Default result"
  end
end

class FirstTask < Base  
  DIAPASON_OF_RAND_NUMBERS = (-10..10)
  
  def initialize(n, m)
    @arrayForFirstTask = Array.new(n) {Array.new(m){rand(DIAPASON_OF_RAND_NUMBERS)}}
  end
  
  def executeTask
    puts "---------------------------------------First task---------------------------------------";
    @arrayForFirstTask.each {|elements| puts "#{elements}"} #puts "#{@arrayForFirstTask}"
    sum = 0
    puts "Result:"
    @arrayForFirstTask.flatten.each {|element| (sum += element; puts "#{element}") if element < 0}
    puts "Sum = #{sum}" 
  end
end

class SecondTask < Base  
  DIAPASON_OF_RAND_TEMPERATURE = (-40..40)
  TWELVE_MONTH = 12
  
  def initialize(arrayLength)
    @arrayForSecondTask = Array.new(arrayLength) {"#{rand((Time.now - 60*60*24*365)..Time.now).strftime("%d.%m")} #{rand(DIAPASON_OF_RAND_TEMPERATURE)}"}
  end

  def executeTask
    puts "---------------------------------------Second task---------------------------------------";
    puts "#{@arrayForSecondTask}\n\nResult:"
     TWELVE_MONTH.times do |monthNumber| 
       temperatureSum = 0
       countElementsInArrayByPattern = 0;    
       @arrayForSecondTask.map do |element|
 	(temperatureSum+= element.split(" ")[1].to_i; countElementsInArrayByPattern += 1; ) if /\.#{monthNumber + 1}\s|\.0#{monthNumber + 1}\s/ =~ element
       end 
       puts "Month [#{monthNumber+1}], avg temperature = #{(temperatureSum.to_f/countElementsInArrayByPattern).round(2)}" if countElementsInArrayByPattern > 0
     end 
  end
end

class ThirdTask < Base  
  DEFAULT_HASHMAP = {'yes' => 23, 'b' => 'travel', 'yesterday' => 34, 5 => '234', :yesss => :fg, try: 30, key: 'some value', 'yesterday1' => 34, 'yesteryear' => 2014}
  
  def initialize(hashMap=DEFAULT_HASHMAP)
    @hashMapForThirdTask = hashMap
  end

  def executeTask
    puts "---------------------------------------Third task---------------------------------------";
    puts @hashMapForThirdTask
    puts "Result: #{@hashMapForThirdTask.keys.count {|key| key.to_s.start_with?('yes')}}"
  end
end

class FourthTask < Base  
  DEFAULT_HASHMAP = {'yes' => 23, 'b' => 'travel', 'yesterday' => 34, 5=> '234', :yesss => :fg, try: {'yesterday1'=> 34, 'yesteryear' => 2014}, key: [{'yes' => 23, 'b' => 'travel'}], 'yesterday1' => 34, 'yesteryear' => 2014}
  
  def initialize(hashMap=DEFAULT_HASHMAP)
    @hashMapForFourthTask = hashMap
  end

  def executeTask
#     def isHashOrArray?(object)
#       return (object.is_a? (Hash)) || (object.is_a? (Array))
#     end
    puts "---------------------------------------Fourth task---------------------------------------";
    isHashOrArray = lambda{|object| (object.is_a? (Hash)) || (object.is_a? (Array))}
    puts @hashMapForFourthTask
    genCharacterKey = lambda do |h_map| 
      if h_map.is_a? (Array) then
      resultArray = []
      h_map.map do |value|
	value = genCharacterKey.call(value) if isHashOrArray.call(value)
	resultArray << value
      end
      return resultArray
      end
      
      obj = {}
      h_map.map do |key, value|
      key = key.to_sym if key.is_a? (String)      
      value = genCharacterKey.call(value) if isHashOrArray.call(value)
      obj[key] = value 
      end
      obj
    end

    puts "\nResult:\n #{genCharacterKey.call(@hashMapForFourthTask)}"
  end
end


result = [FirstTask.new(4,4), SecondTask.new(40), ThirdTask.new, FourthTask.new]
result.each {|object| object.executeTask}