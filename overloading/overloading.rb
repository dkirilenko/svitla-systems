class Hash
    def +(c)
        self.merge(c)
    end
end

class String
    def -(str)
        self.+(str)
    end
end

h = {"a" => "Den"}
str = "Den"
puts h
puts h + {"a" => "Kirilenko", "r" => "777"}
puts str + "Kirilenko"