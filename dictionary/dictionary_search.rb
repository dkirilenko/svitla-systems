class DictionarySearch
  def initialize(file_path)
    @dictionaryList = {}    
    file = File.new(file_path, "r")
    
    while (line = file.gets)
      word = line.strip
      @dictionaryList[word] = word
    end
    file.close
  end
  
  def word_pairs
    reverseLastCharacters = lambda {|word| word[-1], word[-2] = word[-2], word[-1] ; word}
    resultHash = @dictionaryList.select do |key,value| 
     (@dictionaryList.delete(key); @dictionaryList.delete(value)) if key.length>2 && key[-1] != key[-2] && @dictionaryList.has_key?(reverseLastCharacters.call(value))
    end
    resultHash.to_a
  end
end