module UserSkeleton

	def self.included(base)
		base.instance_eval do 
			include Authenticate
			attr_accessor :new_password, :new_password_confirmation
			
			before_save :before_save
			attr_accessible :email, :first_name, :last_name, :password

			

			validates :email, :first_name, :last_name, presence: true
			validates :email, uniqueness: {message: "Email already exist"}

			validates :new_password, confirmation: true
			validates :email, format: {
				with: /^[-a-zA-Z0-9!#$%&'"*+=?_`{|}~]+(?:\.[-a-z0-9!#$%&'*+=?_`{|}~]+)*@[a-zA-Z0-9][a-zA-Z0-9.-]*[\.]{1}[a-zA-Z]{2,4}$/,
				message: "Set valid email"
			}
		end	
	end

	def before_save
		if !new_password.blank? && !new_password_confirmation.blank?
			self.password = encrypt_password(new_password)
		else
			self.password = @encrypted_password || encrypt_password(self.password)
		end		
	end

	def is_admin?
		false
	end
end