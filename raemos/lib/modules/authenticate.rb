module Authenticate
    def self.included(base)
        base.extend(ClassMethods)
    end

    def encrypt_password(pass)
        @encrypted_password = Digest::MD5.hexdigest(pass)
    end

    module ClassMethods
        def authenticate(email, password)
            user = find_by_email(email)
            user = (user && user.password == Digest::MD5.hexdigest(password)) ? user : nil
        end

        def encrypt(str)
            Digest::MD5.hexdigest(str)
        end
    end
end