class AddEmailIndexToAdmins < ActiveRecord::Migration
  def change
  	add_index :admins, :email
  end
end