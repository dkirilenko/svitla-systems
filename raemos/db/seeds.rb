# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


User.create(first_name: "root", last_name:"root", email:"root@gmail.com", password: Digest::MD5.hexdigest("root"))
User.create(first_name: "1", last_name:"1", email:"1@gmail.com", password: "1")
User.create(first_name: "2", last_name:"2", email:"2@gmail.com", password: "2")
User.create(first_name: "3", last_name:"3", email:"3@gmail.com", password: "3")
User.create(first_name: "4", last_name:"4", email:"4@gmail.com", password: "4")
User.create(first_name: "5", last_name:"5", email:"5@gmail.com", password: "5")








SuperAdmin.create(first_name: "root", last_name:"root", email:"root@gmail.com", password: "root")
SuperAdmin.create(first_name: "1", last_name:"1", email:"1@gmail.com", password: "1")
SuperAdmin.create(first_name: "2", last_name:"2", email:"2@gmail.com", password: "2")
SuperAdmin.create(first_name: "3", last_name:"3", email:"3@gmail.com", password: "3")
SuperAdmin.create(first_name: "4", last_name:"4", email:"4@gmail.com", password: "4")