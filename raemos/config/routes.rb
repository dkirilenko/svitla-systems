Raemos::Application.routes.draw do
  
  resource :admin, except: [:destroy, :show, :edit] do
    get :index
    
    collection do
      get :users, action: "show_users", as: "users"
      get :admins, action: "show_admins", as: "list"
    end
    member do
      delete "/destroy/:id" => "admins#destroy", as: "destroy"
      post "/reset_admin_password/:id" => "admins#reset_admin_password", as: "reset_password"  
    end 
    
    get "/show" => "admins#show", as: "show"        
  end

  get "home/index"

  resource :user, except: [:new, :create, :destroy, :show, :edit] do
    get :index
    post "create" => "users#create", as: "create"
    member do
      delete "destroy/:id" => "users#destroy", as: "destroy"
    end
  end

  post "session/authorize" => "session#authorize", as: "authorize"
  get "session/logout" => "session#logout", as: "logout"


  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  root to: "home#index"

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
