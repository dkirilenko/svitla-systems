class UsersController < ApplicationController
  load_and_authorize_resource

  def index
    @users = User.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @users }
    end
  end

  def create
    @user = User.new(params[:user])
    @user.encrypt_password(@user.password)          


    result_json = {status: "done"}

    if @user.save
      session[:user_id] = @user.id
    else
      result_json = {status: "error", message: @user.errors}
    end

    render json: result_json
  end

  def update
    @user = User.authenticate(current_user.email, params[:user][:password])

    @errors = [];
    if @user 
      @user.new_password_confirmation = params[:new_password_confirmation]
      @user.new_password = params[:new_password]  

      if !@user.update_attributes(params[:user])
        @errors = @user.errors
      end
    else
      @errors = ["Password is incorrect"]
    end
  end

  def destroy    
    @user = User.find(params[:id])
    @user.destroy
   
    render json: {}
  end
end
