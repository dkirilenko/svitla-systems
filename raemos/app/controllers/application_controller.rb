class ApplicationController < ActionController::Base
  protect_from_forgery
  helper_method :current_user

  layout :determine_layout


  rescue_from CanCan::AccessDenied do |exception|
    flash[:error] = "Access denied."
    redirect_to root_url
  end


  private

  def determine_layout
    current_user ? "user" : "application"
  end

  def current_user
    @current_user ||= (session[:is_admin]? Admin.find(session[:user_id]) : User.find(session[:user_id])) if session[:user_id]
  end

  private 

  def add_user_to_session(user_id, is_admin)
    session[:user_id] = user_id
    session[:is_admin] = is_admin
  end

  def remove_user_from_session
    session.delete(:user_id)
    session.delete(:is_admin)
  end
end
