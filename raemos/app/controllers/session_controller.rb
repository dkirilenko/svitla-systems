class SessionController < ApplicationController

	def authorize
		@user = params[:is_admin] == "true" ? Admin.authenticate(params[:email], params[:password]) : User.authenticate(params[:email], params[:password])
		
		result_json = {status: "done"}
		if @user
			add_user_to_session(@user.id, @user.is_admin?)
		else 
			result_json = {
				status: "error",
				message: ["Login or pass is incorrect"]
			}
			
		end
		
		render json: result_json
	end

	def logout
		remove_user_from_session
		redirect_to root_path		
	end
end