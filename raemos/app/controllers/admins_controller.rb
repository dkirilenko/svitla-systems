class AdminsController < ApplicationController
  load_and_authorize_resource
  
  def index
  end

  def show
    @admin = admin_user
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @admin }
    end
  end

  def new
    @admin = Admin.new
  end

  def create
    @admin = params[:isSuper] ? SuperAdmin.new(params[:admin]) : SimpleAdmin.new(params[:admin]); 
    @admin.encrypt_password(@admin.password)
    @admin.save

    redirect_to action: "index"
  end

  def update
    @user = Admin.authenticate(current_user.email, params[:admin][:password])
    @errors = [];
    if @user 
      @user.new_password_confirmation = params[:new_password_confirmation]
      @user.new_password = params[:new_password]  

      if !@user.update_attributes(params[:admin])
        @errors = @user.errors
      end
    else
      @errors = ["Password is incorrect"]
    end
  end

  def destroy
    @admin = Admin.find(params[:id])
    @admin.destroy
    if @admin && @admin.id == current_user.id
      remove_user_from_session
      redirect_to root_path
    end

    render json: {}
  end

  def show_users
    @sort_column = params[:column] || "first_name"
    @direction = params[:direction] == "desc"? "asc" : "desc";
    @users = User.order("#{@sort_column} #{@direction}")
  end

  def show_admins
    @sort_column = params[:column] || "first_name"
    @direction = params[:direction] == "desc"? "asc" : "desc";
    @admins = Admin.order("#{@sort_column} #{@direction}")
  end

  def reset_admin_password
    @admin = Admin.find(params[:id])
    if @admin
      @admin.update_attribute(:password, ""); #empty pass hash
    end

    render json: {}
  end

  private 

  def add_user_to_session(user_id)
    session[:admin_user_id] = user_id;
  end 

  def remove_user_from_session()
    session.delete(:admin_user_id)
  end
end