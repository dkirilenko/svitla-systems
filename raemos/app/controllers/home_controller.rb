class HomeController < ApplicationController
  def index
  	if current_user
	  	if @current_user.is_admin?
	  		redirect_to controller: "admins"
	  	else
	  		redirect_to controller: "users"
	  	end
	end
  end
end
