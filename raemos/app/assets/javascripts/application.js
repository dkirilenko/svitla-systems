// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require_tree .




$(document).ready(function() {
    $(".closePopupBtn").click(hideDioPopups)
    $(".delete_user").click(function(){
        var link = $(this);
        $.ajax({
          type: "DELETE",
          url: link.attr("href")
        }).done(function( msg ) {
            link.parents('tr').remove();
        });
        return false;
    });
    $(".delete_admin").click(function(){
        var link = $(this);
        $.ajax({
          type: "DELETE",
          url: link.attr("href")
        }).done(function( msg ) {
            link.parents('tr').remove();
        });
        return false;
    });
})

function showAuthenicatePopup() {
    showDioDialog({
        id: "signinPopup"
    })
    return false;
}

function showRegistrationPopup() {
    showDioDialog({
        id: "registrationPopup"
    })
    return false;
}

function showEditUserPopup() {
    $('#editUserPopup input[type="password"').val("");
    showDioDialog({
        id: "editUserPopup"
    })
    return false;
}


function registerNewUser(element) {
    var url = $(element).attr("href");
    var first_name = $('#reg_first_name').val()
    var last_name = $('#reg_last_name').val()
    var email = $('#reg_email').val()
    var password = $('#reg_password').val()
    var confirmPassword = $('#reg_confirm_password').val()
    var errorMessage = "";
    var isValid = true;
    $('.errorMessage').text("");

    

    if (stringIsEmpty(password)) {
        $('#password').addClass('borderRed')
        errorMessage = "Set password"
        isValid = false;
    }

    /*
    if (stringIsEmpty(first_name)) {
        $('#reg_first_name').addClass("borderRed");
    } else if (stringIsEmpty(last_name)) {
        $('#reg_last_name').addClass("borderRed");
    } else if (stringIsEmpty(email)) {
        $('#reg_email').addClass("borderRed");
    } else if (stringIsEmpty(password)) {
        $('#reg_password').addClass("borderRed");
    }
    */
    if (password != confirmPassword) {
        $('#reg_password, #reg_confirm_password').addClass('borderRed')
        errorMessage = "Password doesn't match with confirm password"
        isValid = false;
    }

    var data = {
        user : {
            first_name : first_name,
            last_name : last_name,
            email : email,
            password : password
        }
    }
    if (isValid) {
        $.post(url, data, function(data) {
            if (data.status == "done") {

                alert("Done")
                window.location = "/"
            } else {
                $.each(data.message, function(index, message) {
                    errorMessage += message + " "
                })

                $('.errorMessage').text(errorMessage);
            }
        })
    } else {
        $('.errorMessage').text(errorMessage);
    }

}



function saveUserData() {
    var username = $('#username').val()
    var first_name = $('#first_name').val()
    var last_name = $('#last_name').val()
    var email = $('#email').val()
    var password = $('#password').val()
    var newPassword = $('#new_password').val()
    var confirmPassword = $('#confirm_password').val()
    var errorMessage = "";
    var isValid = true;
    $('.errorMessage').text("");

    if (newPassword != confirmPassword) {
        $('#new_password, #confirm_password').addClass('borderRed')
        errorMessage = "New password does not equals confirm password"
        isValid = false;
    }

    if (stringIsNotEmpty(password)) {
        $('#password').addClass('borderRed')
        errorMessage = "Set password"
        isValid = false;
    }

    var data = {
        username : username,
        first_name : first_name,
        last_name : last_name,
        email : email,
        password : password,
        new_password : newPassword,
    }
    if (isValid) {
        $.post("/users/update", data, function(data) {
            if (data.status == "done") {
                $('#new_password, #confirm_password, #password').val("")
                alert("Done")
            } else {
                $.each(data.message, function(index, message) {
                    errorMessage += message + " "
                })

                $('#errorMessage').text(errorMessage);
            }
        })
    }
    $('#errorMessage').text(errorMessage);

}

function stringIsEmpty(str) {
    return !str || !($.type(str) === 'string') || str.length == 0
}

function stringIsNotEmpty(str) {
    return !stringIsEmpty(str);
}

function authorizeUser() {
   var email = $('#authorize_email_field').val();
   var password = $('#authorize_password_field').val();
   var authorizePath = $("#authorize_btn").attr("authorize_path");
   var is_admin = $('#is_admin').is(':checked')
   var errorMessage = "";
   var isValid = true;
   $('.errorMessage').text("");

   /*if (stringIsEmpty(email) || stringIsEmpty(password)) {
        errorMessage = "Email or password is empty!";
        isValid = false;
   }*/

   if (isValid) {
       $.post(authorizePath, {password: password, email: email, is_admin: is_admin}, function(data) {
            if (data.status == "done") {
                window.location.href = "/"
            } else {
                $.each(data.message, function(index, message) {
                    errorMessage += message + " "
                })

                $('.errorMessage').text(errorMessage); 
            }
       });
   } else {
      $('.errorMessage').text(errorMessage);  
   }
}


