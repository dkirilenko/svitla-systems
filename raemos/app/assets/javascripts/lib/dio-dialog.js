function DioDialog(option) {
    var option = option;
    var popup

    this.createPopup = function() {
        popup = $("<div>").attr('id', option.id).addClass('dioBackgroundOverlay');
        popupDiv = $("<div>").addClass('dio_popup_html windowStyle').css({
            'margin-left': -option.width / 2,
            'margin-top': -option.height / 2
        }).html(option.html)
        popup.append(popupDiv);
        popup.insertAfter("body")

    }

    this.show = function() {
        popup.css('display', 'block');
    }

    this.createPopup()
}

function showDioDialog(options) {
    hideDioPopups();
    var default_options = {
        width: 100,
        height: 100
    }
    $.extend(default_options, options);

    $('.dioBackgroundOverlay').show();
    var popup = $('#' + options.id);
    popup.show()
    popup.css({
        'margin-left': - popup.width() / 2,
        'margin-top': - popup.height() / 2
    });
}

function hideDioPopups() {
    $('.dioPopup').hide();
    $('.dioBackgroundOverlay').hide();
}