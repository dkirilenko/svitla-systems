class Admin < ActiveRecord::Base
  include UserSkeleton

  def is_super_admin?
    self.class.name == "SuperAdmin"
  end

  def is_admin?
    true
  end
end
