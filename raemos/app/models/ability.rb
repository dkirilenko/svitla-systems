class Ability
  include CanCan::Ability

  def initialize(user)

    if user
    	if user.is_admin?
    		if user.is_super_admin?
    			can :manage, :all	
    		else
    			can :manage, Admin
    			cannot [:create, :show_admins, :reset_admin_password], Admin
    			can :destroy, Admin do |destroy_admin|
    				destroy_admin == user
    			end
                can :destroy, User
    		end

    	else
    		can :manage, User
    		can :destroy, User do |destroy_user|
    			destroy_user == user
    		end
            can :update, User do |update_user|
                update_user == user
            end
    	end
    end

  end
end
